/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personne;

import hmap.HMapAnnotation;
import java.sql.Date;
import java.util.Vector;
import utilitaire.AttributePass;
import utilitaire.ModelView;

/**
 *
 * @author boaykely
 */
public class Personne {
    
    
    String nom;
    String prenom;
    int age;
    Date datenaissance;
    
    public Personne(){}

    public Personne(String nom, String prenom, int age, Date datenaissance) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.datenaissance = datenaissance;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }
    
   @HMapAnnotation(getUrl = "lister")
   public ModelView getPersonne(){
       ModelView modelview= new ModelView();
       Vector<AttributePass>pass=new Vector<AttributePass>();
       Vector<Personne> vect=new Vector<Personne>();
       Personne p1=new Personne("Rakoto","Rabe",23,Date.valueOf("2000-01-15"));
       Personne p2=new Personne("RakotoZaka","RabeZAKA",30,Date.valueOf("1995-12-15"));
       Personne p3=new Personne("RakotoRABE","RabeSON",23,Date.valueOf("2000-10-25"));
       Personne p4=new Personne("Robert","Tanjona",23,Date.valueOf("2000-05-15"));
       vect.add(p1);
       vect.add(p2);
       vect.add(p3);
       vect.add(p4);
       modelview.setPageName("Acceuille.jsp");
       AttributePass att=new AttributePass();
       att.setKey("personne");
       att.setVal(vect);
       pass.add(att);
       modelview.setAttribute(pass);
       return modelview;
   }
   
   @HMapAnnotation(getUrl = "Save")
   public ModelView Save(){
       ModelView modelview= new ModelView();
       Vector<AttributePass>pass=new Vector<AttributePass>();
       AttributePass att=new AttributePass();
       att.setKey("personne");
       att.setVal(this);
       pass.add(att);
       
       modelview.setAttribute(pass);
       modelview.setPageName("PersonneOne.jsp");
       return modelview;
   }
   
}

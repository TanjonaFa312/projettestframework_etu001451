<%-- 
    Document   : Acceuille
    Created on : Nov 18, 2022, 2:59:51 AM
    Author     : boaykely
--%>

<%@page import="personne.Personne"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<Personne>personne=(Vector<Personne>)(request.getAttribute("personne"));
    //out.println(personne.size());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste Personne</h1>
        <table border="1">
            <tr>
                <td>Nom</td>
                <td>Prenom</td>
                <td>age</td>
                <td>Date de Naissance</td>
            </tr>
            <% for(int i=0;i<personne.size();i++){%>
            <tr>
                <td><% out.print(personne.elementAt(i).getNom()); %></td>
                <td><% out.print(personne.elementAt(i).getPrenom()); %></td>
                <td><% out.print(personne.elementAt(i).getAge()); %></td>
                <td><% out.print(personne.elementAt(i).getDatenaissance()); %></td>
            </tr>
            <% } %>
        </table>
    </body>
</html>
